from common.json import ModelEncoder
from .models import Technician, AutomobileVO,  Appointment


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
    ]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "vin",
        "date_time",
        "reason",
        "status",
        "customer",
        "technician",
    ]
    encoders = {
        "technician": TechnicianEncoder()
    }
