import { useEffect, useState } from 'react';

function AppointmentList() {
    const [appointments, setAppointments] = useState([]);
    const [vips, setVips] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/');
        const vipResponse = await fetch('http://localhost:8100/api/automobiles/');
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments)
        }
        if (vipResponse.ok){
            const vipData = await vipResponse.json();
            setVips(vipData.autos)
        }
    }

    useEffect(() => {
        getData()
    }, [])

    const checkVip = (app) => {
        for(const vip of vips) {
            if (app.vin === vip.vin) {
                return true;
            }
        }
        return false;
    }

    const handleFinish = async (id) => {
        const fetchConfig = {
            method: "PUT",
            "Content-Type": "applications/json",
        }
        const response = await fetch(`http://localhost:8080/api/appointments/${id}/finish/`, fetchConfig)
        if (response.ok) {
            setAppointments((appointment) => appointment.filter((app) => app.id !== id)
            )
        }
    }
    const handleCancel = async (id) => {
        const fetchConfig = {
            method: "PUT",
            "Content-Type": "applications/json",
        }
        const response = await fetch(`http://localhost:8080/api/appointments/${id}/cancel/`, fetchConfig)
        if (response.ok) {
            setAppointments((appointment) => appointment.filter((app) => app.id !== id)
            )
        }
    }

    return (
        <div className="container">
            <h1 className='display-5 fw-bold mt-4 mb-4 d-flex justify-content-center'>Service Appointments</h1>
            <table className="table table-striped table-dark">
                <thead>
                    <tr>
                        <th>Vin</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>VIP</th>
                        <th>Finish / Cancel</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.filter((app)=> app.status === "created").map(app => {
                        return (
                            <tr key={app.id}>
                                <td>{app.vin}</td>
                                <td>{app.customer}</td>
                                <td>{new Date(app.date_time).toDateString()}</td>
                                <td>{new Date(app.date_time).toLocaleTimeString()}</td>
                                <td>{app.technician.first_name} {app.technician.last_name}</td>
                                <td>{app.reason}</td>
                                <td>{checkVip(app) ? 'Yes' : 'No'}</td>
                                <td>
                                <button id={app.id} onClick={() => handleFinish(app.id)} className="btn btn-success">Finish</button>
                                <button id={app.id} onClick={() => handleCancel(app.id)} className="btn btn-danger">Cancel</button>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default AppointmentList;
