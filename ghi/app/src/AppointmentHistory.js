import { useEffect, useState } from 'react';

function AppointmentHistory() {
    const [appointments, setAppointments] = useState([]);
    const [vips, setVips] = useState([])
    const [search, setSearch] = useState('');

    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/');
        const vipResponse = await fetch('http://localhost:8100/api/automobiles/');
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments)
        }
        if (vipResponse.ok){
            const vipData = await vipResponse.json();
            setVips(vipData.autos)
        }
    }
    useEffect(() => {
        getData()
    }, [])
    const checkVip = (app) => {
        for(const vip of vips) {
            if (app.vin === vip.vin) {
                return true;
            }
        }
        return false;
    }


    return (
        <div className="container">
            <h1 className='display-5 fw-bold mt-4 mb-4 d-flex justify-content-center'>Service History</h1>
            <form onChange={(e) => setSearch(e.target.value)} className="d-flex mb-4" role="search">
                <input className="form-control me-2" type="search" placeholder="Search by vin" aria-label="Search" />
                {/* <button className="btn btn-outline-success" type="submit">Search</button> */}
            </form>
            <table className="table table-striped table-dark">
                <thead>
                    <tr>
                        <th>Vin</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>VIP</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.filter((app) => {
                        return  search.toLowerCase() === '' ? app : app.vin.toLowerCase().includes(search.toLowerCase())
                    }).map(app => {
                        return (
                            <tr key={app.id}>
                                <td>{app.vin}</td>
                                <td>{app.customer}</td>
                                <td>{new Date(app.date_time).toLocaleDateString()}</td>
                                <td>{new Date(app.date_time).toLocaleTimeString()}</td>
                                <td>{app.technician.first_name} {app.technician.last_name}</td>
                                <td>{app.reason}</td>
                                <td>{checkVip(app) ? 'Yes' : 'No'}</td>
                                <td>{app.status}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default AppointmentHistory;
