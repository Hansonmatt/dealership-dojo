import React, { useState } from 'react';

function CustomerForm() {
    const [address, setAddress] = useState('')
    const [last_name, setLastName] = useState('')
    const [first_name, setFirstName] = useState('')
    const[phone_number,setPhone] = useState('')
    const [submitted, setSubmitted]= useState(false)

    const handleFirstNameChange = (e) => {
        const value = e.target.value;
        setFirstName(value);
    }
    const handleLastNameChange = (e) => {
        const value = e.target.value;
        setLastName(value)
    }
    const handleAddressChange = (e) => {
        const value = e.target.value;
        setAddress(value);
    }
    const handlePhoneChange = (e) => {
        const value = e.target.value;
        setPhone(value);
    }

    const handleSubmit = async (e) => {
        e.preventDefault();

        const data = {
            first_name,
            last_name,
            address,
            phone_number,
        }

        console.log(data)

        const customersURL = 'http://localhost:8090/api/customers/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const customerResponse = await fetch(customersURL, fetchConfig)
        if (customerResponse.ok) {
            const customer = await customerResponse.json()
            console.log(customer)
            setFirstName('')
            setLastName('')
            setAddress('')
            setPhone('')
            setSubmitted(true)
        }
    }
    const messageClasses = (!submitted) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';
    return (
        <div className="row">
            <h1 className='d-flex justify-content-center mt-5'>Create a Customer</h1>
            <div className="offset-3 col-6 bg-dark">
                <div className="shadow p-4 mt-4">
                    <form onSubmit={handleSubmit} id="create-hat-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFirstNameChange} value={first_name} placeholder="First Name" required type="text" name="first_name" id="first_name" className="form-control" />
                            <label htmlFor="first_name">First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleLastNameChange} value={last_name} placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control" />
                            <label htmlFor="last_name">Last Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleAddressChange} value={address} placeholder="Address" required type="text" name="address" id="address" className="form-control" />
                            <label htmlFor="address">Address</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePhoneChange} value={phone_number} placeholder="Phone Number" required type="text" name="phone_number" id="phone_number" className="form-control" />
                            <label htmlFor="phone_number">Phone Number</label>
                        </div>
                        <button className="btn btn-outline-light">Create</button>
                    </form>
                </div>
                <div className={messageClasses} role="alert">
            Customer created!
          </div>
            </div>
        </div>
    )

}

export default CustomerForm;
