import { useNavigate } from "react-router-dom";

function MainPage() {
  const navigate = useNavigate();


  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">Dealership Dojo</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          The premiere solution for automobile dealership
          management!
        </p>
        <div className="d-grid gap-2 col-8 mx-auto">
          <button
            onClick={() => navigate("/automobiles")}
            className="btn btn-outline-light mx-2"
          >
            Automobile Inventory
          </button>
          <button
            onClick={() => navigate("/appointments/create")}
            className="btn btn-outline-light mx-2"
          >
            Schedule Service Appointment
          </button>
        </div>
      </div>
    </div>
  );
}

export default MainPage;
