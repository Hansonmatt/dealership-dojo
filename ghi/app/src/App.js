import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturerForm from './ManufacturerForm';
import ListModels from './ListModels';
import ModelForm from './ModelForm';
import ListManufacturer from './ListManufacturer';
import AutomobileForm from './AutomobileForm';
import AutomobileList from './AutomobileList';
import TechnicianForm from './TechnicianForm';
import TechnicianList from './TechnicianList';
import AppointmentForm from './AppointmentForm';
import SalespeopleList from './SalespeopleList';
import SalespersonForm from './SalespersonForm';
import CustomerList from './CustomerList';
import CustomerForm from './CustomerForm';
import SalesList from './SalesList';
import SalesForm from './SalesForm';
import UnsoldAutos from './UnsoldAutos';
import SalespersonHistory from './SalespersonHistory'
import AppointmentList from './AppointmentList';
import AppointmentHistory from './AppointmentHistory';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers">
            <Route index element={<ListManufacturer />} />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="models">
            <Route index element={<ListModels />} />
            <Route path="new" element={<ModelForm />} />
          </Route>
          <Route path="automobiles">
            <Route index element={<AutomobileList />} />
            <Route path="unsold" element={<UnsoldAutos/>}/>
            <Route path="create" element={<AutomobileForm />} />
          </Route>
          <Route path="technicians">
            <Route index element={<TechnicianList />} />
            <Route path="create" element={<TechnicianForm />} />
          </Route>
          <Route path="appointments">
            <Route index element={<AppointmentList />} />
            <Route path="create" element={<AppointmentForm />} />
            <Route path="history" element={<AppointmentHistory />} />
          </Route>
          <Route path="salespeople">
            <Route index element={<SalespeopleList/>}/>
            <Route path="history" element={<SalespersonHistory/>}/>
            <Route path="new" element={<SalespersonForm/>}/>
          </Route>
          <Route path="customers">
            <Route index element={<CustomerList/>}/>
            <Route path="new" element={<CustomerForm/>}/>
          </Route>
          <Route path="sales">
            <Route index element={<SalesList/>}/>
            <Route path="new" element={<SalesForm />} />
            {/* <Route path="new" element={<SalesForm/>}/> */}
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
