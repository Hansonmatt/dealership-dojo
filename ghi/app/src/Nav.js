import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <div className="container-fluid">
        <NavLink className="navbar-brand fs-4" to="/">Dealership Dojo</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNavDropdown">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0 d-flex justify-content-end">
            <li className="nav-item dropdown fs-4 px-4">
              <a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Manufacturers
              </a>
              <ul className="dropdown-menu">
                <li className="nav-item fs-5 px-4">
                  <NavLink className="dropdown-item" to="/manufacturers">Manufacturers</NavLink>
                </li>
                <li className="nav-item fs-5 px-4">
                  <NavLink className="dropdown-item" to="manufacturers/new">Create a Manufacturer</NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item dropdown fs-4 px-4">
              <a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Models
              </a>
              <ul className="dropdown-menu">
                <li className="nav-item fs-5 px-4">
                  <NavLink className="dropdown-item" to="/models">Models</NavLink>
                </li>
                <li className="nav-item fs-5 px-4">
                  <NavLink className="dropdown-item" to="/models/new">Create a model</NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item dropdown fs-4 px-4">
              <a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Automobiles
              </a>
              <ul className="dropdown-menu">
                <li className="nav-item fs-5 px-4">
                  <NavLink className="dropdown-item" to="/automobiles">Automobiles</NavLink>
                </li>
                <li className="nav-item fs-5 px-4">
                  <NavLink className="dropdown-item" to="/automobiles/create">Create an Automobile</NavLink>
                </li>
                <li className="nav-item fs-5 px-4">
                  <NavLink className="dropdown-item" to="/automobiles/unsold">Unsold Automobiles</NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item dropdown fs-4 px-4">
              <a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Technicians
              </a>
              <ul className="dropdown-menu">
              <li className="nav-item fs-5 px-4">
                <NavLink className="dropdown-item" to="/technicians">Technicians</NavLink>
              </li>
              <li className="nav-item fs-5 px-4">
                <NavLink className="dropdown-item" to="/technicians/create">Add a Technician</NavLink>
              </li>
              </ul>
            </li>
            <li className="nav-item dropdown fs-4 px-4">
              <a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Car Service
              </a>
              <ul className="dropdown-menu">
                <li className="nav-item fs-5 px-4">
                  <NavLink className="dropdown-item" aria-current="page" to="/appointments/create">Schedule a Service Appointment</NavLink>
                </li>
                <li className="nav-item fs-5 px-4">
                  <NavLink className="dropdown-item" aria-current="page" to="/appointments">Service Appointments</NavLink>
                </li>
                <li className="nav-item fs-5 px-4">
                  <NavLink className="dropdown-item" aria-current="page" to="/appointments/history">Service History</NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item dropdown fs-4 px-4">
              <a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Sales
              </a>
              <ul className="dropdown-menu">
                <li className="nav-item fs-5 px-4">
                  <NavLink className="dropdown-item" aria-current="page" to="/sales">Sales</NavLink>
                </li>
                <li className="nav-item fs-5 px-4">
                  <NavLink className="dropdown-item" aria-current="page" to="/sales/new">Record a new sale</NavLink>
                </li>
                <li className="nav-item fs-5 px-4">
                  <NavLink className="dropdown-item" aria-current="page" to="/salespeople">Salespeople</NavLink>
                </li>
                <li className="nav-item fs-5 px-4">
                  <NavLink className="dropdown-item" aria-current="page" to="/salespeople/history">Salesperson History</NavLink>
                </li>
                <li className="nav-item fs-5 px-4">
                  <NavLink className="dropdown-item" aria-current="page" to="/salespeople/new">Create a Salesperson</NavLink>
                </li>
              </ul>
            </li>
            <li className="nav-item dropdown fs-4 px-4">
              <a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Customers
              </a>
              <ul className="dropdown-menu">
              <li className="nav-item fs-5 px-4">
                  <NavLink className="dropdown-item" aria-current="page" to="/customers">Customers</NavLink>
                </li>
                <li className="nav-item fs-5 px-4">
                  <NavLink className="dropdown-item" aria-current="page" to="/customers/new">Create a Customer</NavLink>
                </li>
            </ul>
          </li>
          </ul>
        </div>
      </div>

    </nav>
  )
}

export default Nav;
