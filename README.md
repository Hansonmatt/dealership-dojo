# Dealership Dojo

Dealership Dojo is a single-page web application for managing different
aspects of automobile dealerships. It can manage
automobile manufacturer information, automobile models and inventory,
technicians, service appointments, salespeople,
customers, and sales. Dealership Dojo boasts a user-friendly interface,
built with React.

Team:

* Josh Lao - Sales

* Matthew Hanson - Automobile Service

## Running the Project
* Make sure you have Docker, Gitlab, and Node.js v18.2 or later.

1. Fork this repository: https://gitlab.com/Hansonmatt/dealership-dojo
2. In terminal, cd into the directory where you would like to clone the project.
3. Clone the forked repository onto your machine in terminal with the following command:
	git clone <<yourrespository.url.here>>
4. Run the following commands in terminal:
```
docker volume create beta-data
docker compose build
docker compose up
```
5. It will take time for the commands to run so be patient,
   once the application is bound, you will be able to view Dealership Dojo at 'localhost:3000'

## Project Diagram
images\Project Diagram.png

## Service microservice description
The service microservice has three models: A Technician model, an AutomobileVo model, and an Appointment model. While my models file includes an AutomobileVO model with a vin field, it is not used as a Foreign Key for the vin field in my Appointment model. The service directory includes a poll directory, inside the poll directory there is a file named 'poller.py. Inside of this file, you will find the logic that I used to retrieve required information (vin and sold fields) from the inventory microservcice.

The overall purpose of the Service microservice is to maintain a list technicians, monitor the status of scheduled service appointments, and maintain a service history log where appointments can be queried by a vehicle's vin number. On top of those features, the service microservice also allows the addition of new technicians and an appointment scheduling form expediting the appointment creation process.

## Service API Endpoints

## Technicians
Technicians have three API endpoints: GET, POST, and DELETE. You may access these endpoints through your browser or insomnia.

'GET' A list of technicians: http://localhost:8080/api/technicians/
You do not have to provide a JSON body to access this information. You will recieve a list with the following structure:
```
Example:
{
	"technicians": [
		{
			"id": 1,
			"first_name": "Matthew",
			"last_name": "Hanson",
			"employee_id": "mhanson"
		},
    ]
}
```

'POST' Create a new Technician: http://localhost:8080/api/technicians/
When creating a new technicain you WILL need to inclide a JSON body to post new information. Please refer to the following structure:
```
Example JSON Body:
{
	"first_name": "Sponge",
	"last_name": "Bob",
	"employee_id": "sbob"
}

Example Response Returned:
{
	"id": 3,
	"first_name": "Sponge",
	"last_name": "Bob",
	"employee_id": "sbob"
}
```

The id paramater that is returned in the response is automatically generated and will be used when deleting a technician or accessing the information on a webpage.



'DELETE' delete a tehcnician: http://localhost:8080/api/technicians/<int:pk>/
When deleting a technician the only thing you will need is the id of the employee, you will replace '<int:pk>' with the id and will recieve the following response:

```
Example:
{
	"message": "Technician has been deleted"
}

```


This completes the list of API endpoints available for Technicians.


## Appointment API Endpoints
Appointments have five API endpoints including GET, POST, DELETE, PUT and PUT. You will notice that their are two PUT methods, and that is not a mistake. One of the PUT methods will be used to update an appointments status to finished and the other will set the status to cancelled. You may access these endpoints through your browser or insomnia.

'GET' A list of apppointments: http://localhost:8080/api/appointments/
This will return a list of all appointments in the database.

```
Example:
{
	"appointments": [
		{
			"id": 1,
			"vin": "1C3CC5FB2AN120174",
			"date_time": "2023-12-31T18:00:00+00:00",
			"reason": "Exhaust",
			"status": "finished",
			"customer": "Customer Customer",
			"technician": {
				"id": 1,
				"first_name": "Matthew",
				"last_name": "Hanson",
				"employee_id": "mhanson"
			}
		},
    ]
}

```

'POST' Create a new appointment: http://localhost:8080/api/appointments/
You will need to include a JSON body to be able to post the information for a new appointment. By now you have propbably noticed that when a list of appointments is requested, the technician field diaplays all of the technician's information. You will not need that to create an appointment, you will only need the technician's id.

```
Example JSON body:
{
	"date_time": "2023-12-30T18:00:00+00:00",
	"reason": "Exhaust",
	"vin": "JH4DC4460XS001340",
	"customer": "Customer Customer",
	"technician_id": 1
}

Example response:
{
	"appointments": [
		{
			"id": 1,
			"vin": "1C3CC5FB2AN120174",
			"date_time": "2023-12-31T18:00:00+00:00",
			"reason": "Exhaust",
			"status": "finished",
			"customer": "Customer Customer",
			"technician": {
				"id": 1,
				"first_name": "Matthew",
				"last_name": "Hanson",
				"employee_id": "mhanson"
			}
		},
    ]
}
```
Using only the technican's id will return all available information for that technician in the appointments information.

'DELETE' an Appointment: http://localhost:8080/api/appointments/<int.pk>
When deleting an appointment the only thing you will need is the id of the appointment, you will replace '<int:pk>' with the id and will recieve the following response:

```
{
	"message": "Appointment has been deleted"
}
```

'PUT' to set an Appointment's status to 'finished': http://localhost:8080/api/appointments/<int:pk>/finish/
You will not need a JSON body for this method, the only thing that is needed is the specific appointment id that you wish to mark as finished. Just replace <int:pk> with the id of your choice.

```
Example:
{
	"id": 1,
	"date_time": "2023-12-31T18:00:00+00:00",
	"reason": "Exhaust",
	"status": "finished",
	"customer": "Customer Customer",
	"technician": {
		"id": 1,
		"first_name": "Matthew",
		"last_name": "Hanson",
		"employee_id": "mhanson"
	}
}
```
Note that the status of appointment with an id of 1, now has a status marked as finished.

'PUT' to set an Appointment's status to 'canceled': http://localhost:8080/api/appointments/<int:pk>/cancel/
You will not need a JSON body for this method, the only thing that is needed is the specific appointment id that you wish to mark as canceled. Just replace <int:pk> with the id of your choice.

```
Example:
{
	"id": 1,
	"date_time": "2023-12-31T18:00:00+00:00",
	"reason": "Exhaust",
	"status": "canceled",
	"customer": "Customer Customer",
	"technician": {
		"id": 1,
		"first_name": "Matthew",
		"last_name": "Hanson",
		"employee_id": "mhanson"
	}
}
```
Note that the status of appointment with an id of 1, now has a status marked as canceled.

This completes the list of API endpoints available for Appointments.



## URL Ports

# Manufacturers URLs:
'GET' List manufacturers:	http://localhost:8100/api/manufacturers/
'POST' Create a manufacturer: http://localhost:8100/api/manufacturers/
'GET' Get a specific manufacturer: http://localhost:8100/api/manufacturers/:id/
'PUT' Update a specific manufacturer: http://localhost:8100/api/manufacturers/:id/
'DELETE' Delete a specific manufacturer: http://localhost:8100/api/manufacturers/:id/

## Manufacturers CRUD Demo
'GET' a list of manufacturers will return the following:
```
Example:
{
	"manufacturers": [
		{
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Chrysler"
		},
    ]
}

```

'POST' Create a manufacture will require a JSON body:
```
Example Json Body:
{
  "name": "Toyota"
}

Example Response:
{
	"href": "/api/manufacturers/2/",
	"id": 2,
	"name": "Toyota"
}

```
Note that the id field is automatically generated.

'GET' Specific information about a manufacturer will require the manufacturers id and return the details of that manufacturer.
```
Example:
{
	"href": "/api/manufacturers/2/",
	"id": 2,
	"name": "Toyota"
}
```

'PUT' update a specific manufacturer. Your JSON body should include anything you want to update. The url should include the manufacturer id.

```
Example:
{
  "name": "Honda"
}

Example Response:
{
	"href": "/api/manufacturers/2/",
	"id": 2,
	"name": "Honda"
}

```

'DELETE' will delete the manufacturer from the database. It does not require a JSON body but will require the id in the URL.
```
Example Response on delete:
{
	"id": null,
	"name": "Toyota"
}
```



# Vehicle Models URLs:
'GET' List vehicle models: http://localhost:8100/api/models/
'POST'Create a vehicle model: http://localhost:8100/api/models/
'GET' Get a specific vehicle model :http://localhost:8100/api/models/:id/
'PUT' Update a specific vehicle model: http://localhost:8100/api/models/:id/
'DELETE' Delete a specific vehicle model: http://localhost:8100/api/models/:id/

## Vehicle Models CRUD demo:
'GET' a list of vehcile models:
```
Example:
{
	"models": [
		{
			"href": "/api/models/1/",
			"id": 1,
			"name": "Sebring",
			"picture_url": "https://shorturl.at/CIKZ3",
			"manufacturer": {
				"href": "/api/manufacturers/1/",
				"id": 1,
				"name": "Chrysler"
			}
		}
	]
}
```

'POST' create a new model, this will require a JSON body, the manufacturer's id, and a url for a photo of the vehicle.
```
Example JSON body:
{
  "name": "Tacoma",
  "picture_url": "https://shorturl.at/wCH45",
  "manufacturer_id": 2
}

Example Response:
{
	"href": "/api/models/2/",
	"id": 2,
	"name": "Tacoma",
	"picture_url": "https://shorturl.at/wCH45",
	"manufacturer": {
		"href": "/api/manufacturers/2/",
		"id": 2,
		"name": "Toyota"
	}
}
```
Note that the response contains the information about a manufacturer.

'GET' Specific details about a model, you will need the id of the model, no JSON body is required.
```
Example Response:
{
	"href": "/api/models/1/",
	"id": 1,
	"name": "Sebring",
	"picture_url": "https://shorturl.at/CIKZ3",
	"manufacturer": {
		"href": "/api/manufacturers/1/",
		"id": 1,
		"name": "Chrysler"
	}
}
```

'PUT' update model feilds, you can update the model name or url but you cannot change the manufacturer.
```
Example JSON body:
{
  "name": "Sebring",
  "picture_url": "https://shorturl.at/CIKZ3"
}
```

'DELETE' Delete a vehicle model from the database, you will need the model's id no JSON body is required.
```
Example:
{
  "models": [
    {
      "href": "/api/models/1/",
      "id": null,
      "name": "Sebring",
      "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
      "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Daimler-Chrysler"
      }
    }
  ]
}
```



# Automobiles URLs:
'GET' List automobiles: http://localhost:8100/api/automobiles/
'POST' Create an automobile: http://localhost:8100/api/automobiles/
'GET' Get a specific automobile: http://localhost:8100/api/automobiles/:vin/
'PUT' Update a specific automobile: http://localhost:8100/api/automobiles/:vin/
'DELETE' Delete a specific automobile: http://localhost:8100/api/automobiles/:vin/

## Automobile CRUD Demo:

'GET' A list of automobiles:
```
Example:
{
	"autos": [
		{
			"href": "/api/automobiles/1C3CC5FB2AN120174/",
			"id": 1,
			"color": "Red",
			"year": 2012,
			"vin": "1C3CC5FB2AN120174",
			"model": {
				"href": "/api/models/1/",
				"id": 1,
				"name": "Sebring",
				"picture_url": "https://shorturl.at/CIKZ3",
				"manufacturer": {
					"href": "/api/manufacturers/1/",
					"id": 1,
					"name": "Chrysler"
				}
			},
			"sold": false
		},
	]
}
```

'POST' create a new autombile, this will require a JSON body and the model's id.
```
Example JSON body:
{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120174",
  "model_id": 1
}

Example response:
{
	"href": "/api/automobiles/1C3CC5FB2AN120174/",
	"id": 1,
	"color": "red",
	"year": 2020,
	"vin": "1C3CC5FB2AN120174",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "Sebring",
		"picture_url": "https://shorturl.at/wCH45",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Chrysler"
		}
	},
	"sold": false
}
```

'GET' Details on a specific Automobile, this will require the autombile's vin number.
```
Example:
{
  "href": "/api/automobiles/1C3CC5FB2AN120174/",
  "id": 1,
  "color": "red",
  "year": 2013,
  "vin": "1C3CC5FB2AN120174",
  "model": {
    "href": "/api/models/1/",
    "id": 1,
    "name": "Sebring",
    "picture_url": "https://shorturl.at/wCH4",
    "manufacturer": {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Chrysler"
    }
  },
  "sold": false
}
```

'PUT' You can update the color, year, and sold status of an automobile, this will require a JSON body and the automobile's vin in the URL.
```
Example JSON body:
{
  "color": "red",
  "year": 2012,
  "sold": true
}
```

'DELETE' Delete a specific automobile, this will require the vehicles vin in the url.
```
Example:
{
	"href": "/api/automobiles/1C3CC5FB2AN120300/",
	"id": null,
	"color": "Grey",
	"year": 2013,
	"vin": "1C3CC5FB2AN120300",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "Sebring",
		"picture_url": "https://shorturl.at/CIKZ3",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Chrysler"
		}
	},
	"sold": false
}
```


## Sales microservices description
Welcome to Sales, where we have 4 models: AutomobileVO, Customer, SalesPerson, and Sale. The Sale rely's on data from the three other models to create a new Sale. A salesperson also has a history of sales from the font-end under Salesperson history where you can filter by the salesperson of your choosing.

The AutomobileVO is a value object that contains the VIN, and the sold attributes polled from the Automobile model from the inventory which is constantly being polled in the poller.py at "sales/poll/poller.py".

## Sales microservice URLS

# Salespeople:
List salespeople: http://localhost:8090/api/salespeople/
Create a salesperson: http://localhost:8090/api/salespeople/
Delete a specific salesperson: http://localhost:8090/api/salespeople/:id/
# CRUD Demo:
Return value of listing salespeople(GET):
```
{
	"salespeople": [
		{
			"href": "/api/salespeople/5",
			"first_name": "Josh",
			"last_name": "Lao",
			"employee_id": 1234567890,
			"id": 5
		},
		{
			"href": "/api/salespeople/6",
			"first_name": "Mike",
			"last_name": "Ross",
			"employee_id": 127854213,
			"id": 6
		}
	]
}
```
Input value of creating a salesperson(POST):
```
{
	"first_name": "Harvey",
	"last_name": "Spector",
	"employee_id": 218937
}
```

Return value of creating a salesperson(POST):
```
{
	"href": "/api/salespeople/8",
	"first_name": "Harvey",
	"last_name": "Spector",
	"employee_id": 218937,
	"id": 8
}
```
Return value of deleting an existing salesperson(DELETE):
URL: http://localhost:8090/api/salespeople/8
Return:
```
{
	"message": "Salesperosn has been deleted"
}
```
# Customers:
List customers: http://localhost:8090/api/customers/
Create a customer: http://localhost:8090/api/customers/
Delete a specific customer: http://localhost:8090/api/customers/:id/

# CRUD Demo:
Return value of listing customers(GET):
```
{
	"customers": [
		{
			"href": "/api/customers/1",
			"first_name": "Custom",
			"last_name": "Mer",
			"address": "1234 Street Lane. Carrollton Texas",
			"phone_number": "123-456-7890",
			"id": 1
		},
		{
			"href": "/api/customers/6",
			"first_name": "Custo",
			"last_name": "Mer",
			"address": "1234 Lane Blvd. City State, 12345",
			"phone_number": "4694637976",
			"id": 6
		}
	]
}
```
Input when creating customer(POST):
```
{
	"first_name": "Custom",
	"last_name": "Mer",
	"address": "1234 Street Lane. Carrollton Texas",
	"phone_number": "123-456-7890"
}
```
Return value of creating a customer(POST):
```
{
	"href": "/api/customers/7",
	"first_name": "Custom",
	"last_name": "Mer",
	"address": "1234 Street Lane. Carrollton Texas",
	"phone_number": "123-456-7890",
	"id": 7
}
```
Return value of deleting an existing customer(DELETE):
URL: http://localhost:8090/api/customers/7
Return:
```
{
	"message": "Customer has been deleted"
}
```
# Sales:
List sales: http://localhost:8090/api/sales/
Create a sale: http://localhost:8090/api/sales/
Delete a sale: http://localhost:8090/api/sales/:id/

# CRUD Demo:
Return value of listing sales(GET):
```
{
	"sales": [
		{
			"href": "/api/sales/13",
			"automobile": {
				"vin": "JH4DB1667MS803278",
				"sold": true,
				"id": 27
			},
			"salesperson": {
				"href": "/api/salespeople/5",
				"first_name": "Josh",
				"last_name": "Lao",
				"employee_id": 1234567890,
				"id": 5
			},
			"customer": {
				"href": "/api/customers/6",
				"first_name": "Custo",
				"last_name": "Mer",
				"address": "1234 Lane Blvd. City State, 12345",
				"phone_number": "4694637976",
				"id": 6
			},
			"price": 9999.0,
			"id": 13
		},
		{
			"href": "/api/sales/14",
			"automobile": {
				"vin": "KM8SB73D244631319",
				"sold": true,
				"id": 28
			},
			"salesperson": {
				"href": "/api/salespeople/5",
				"first_name": "Josh",
				"last_name": "Lao",
				"employee_id": 1234567890,
				"id": 5
			},
			"customer": {
				"href": "/api/customers/1",
				"first_name": "Custom",
				"last_name": "Mer",
				"address": "1234 Street Lane. Carrollton Texas",
				"phone_number": "123-456-7890",
				"id": 1
			},
			"price": 99.99,
			"id": 14
		},
		{
			"href": "/api/sales/15",
			"automobile": {
				"vin": "asdfasdf",
				"sold": true,
				"id": 26
			},
			"salesperson": {
				"href": "/api/salespeople/6",
				"first_name": "Mike",
				"last_name": "Ross",
				"employee_id": 127854213,
				"id": 6
			},
			"customer": {
				"href": "/api/customers/6",
				"first_name": "Custo",
				"last_name": "Mer",
				"address": "1234 Lane Blvd. City State, 12345",
				"phone_number": "4694637976",
				"id": 6
			},
			"price": 1234.99,
			"id": 15
		}
	]
}
```
Input value of creating a new sale(POST):
```
{
	"salesperson_id": 7,
	"customer_id": 6,
	"automobile": "JH4CL95804C000333",
	"price": 9999.99
}

```
Return value of creating a new sale(POST):
```
{
	"href": "/api/sales/16",
	"automobile": {
		"vin": "JH4CL95804C000333",
		"sold": true,
		"id": 33
	},
	"salesperson": {
		"href": "/api/salespeople/7",
		"first_name": "Harvey",
		"last_name": "Spector",
		"employee_id": 23,
		"id": 7
	},
	"customer": {
		"href": "/api/customers/6",
		"first_name": "Custo",
		"last_name": "Mer",
		"address": "1234 Lane Blvd. City State, 12345",
		"phone_number": "4694637976",
		"id": 6
	},
	"price": 9999.99,
	"id": 16
}
```
Return value of deleting a sale(DELETE):
URL: http://localhost:8090/api/sales/16
Return:
```
{
	"message": "Sale has been deleted"
}
```
